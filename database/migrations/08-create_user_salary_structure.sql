CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TABLE user_salary_structure (
    id SERIAL NOT NULL PRIMARY KEY,
    user_id INT REFERENCES users(id) NOT NULL,
    salary_structure_id INT REFERENCES salary_structures(id) NOT NULL,
    salary_structure_start_time DATE NOT NULL,
    salary_structure_end_time DATE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON user_salary_structure
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();