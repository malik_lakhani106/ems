FROM node:12-alpine
WORKDIR /ems-app
COPY . .
EXPOSE 5000
RUN npm install
CMD ["npm", "start"]