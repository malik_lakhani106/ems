const { executeQuery, knex } = require('../providers/db-connections.js');

exports.getLeaves = (itemsPerPage, offset) => {
  const getLeavesQuery = knex('leaves')
    .select('id', 'leave_type', 'created_at', 'updated_at')
    .limit(itemsPerPage)
    .offset(offset);
  return executeQuery(getLeavesQuery)
    .then((leaves) => {
      if (leaves && leaves.rows) {
        return leaves.rows;
      }
    });
};

exports.getLeavesCount = () => {
  const getLeavesCountQuery = knex('leaves').count('*');
  return executeQuery(getLeavesCountQuery)
    .then((leavesCount) => {
      if (leavesCount && leavesCount.rows) {
        return parseInt(leavesCount.rows[0].count, 10);
      }
    });
};

exports.createLeave = (packet) => {
  const createLeaveQuery = knex('leaves')
    .insert({
      leave_type: packet.leave_type,
    })
    .returning(['id', 'leave_type', 'created_at', 'updated_at']);
  return executeQuery(createLeaveQuery)
    .then((leave) => leave.rows[0]);
};

exports.getLeaveByLeaveType = (leaveType) => {
  const getLeaveByLeaveTypeQuery = knex('leaves')
    .select('leave_type')
    .where('leave_type', leaveType)
    .limit(1);
  return executeQuery(getLeaveByLeaveTypeQuery)
    .then((leave) => {
      if (leave && leave.rows.length) {
        return leave.rows[0];
      }
    });
};

exports.deleteLeaveById = (leaveId) => {
  const deleteLeaveByIdQuery = knex('leaves')
    .where('id', leaveId)
    .del();
  return executeQuery(deleteLeaveByIdQuery)
    .then((deletedLeave) => {
      if (!deletedLeave.rows.length) {
        return `Leave having id '${leaveId}' deleted successfully`;
      }
    });
};

exports.updateLeaveById = (leaveType, leaveId) => {
  const updateLeaveByIdQuery = knex('leaves')
    .where('id', leaveId).update({
      leave_type: leaveType,
    },
    ['id', 'leave_type', 'created_at', 'updated_at']);
  return executeQuery(updateLeaveByIdQuery)
    .then((updatedLeave) => {
      if (updatedLeave && updatedLeave.rows && updatedLeave.rows.length) {
        return updatedLeave.rows[0];
      }
    });
};
