const moment = require('moment');
const { executeQuery, knex } = require('../providers/db-connections.js');

exports.assignSalaryStructToUser = (userId, salaryStructId, packet) => {
  const assignSalaryStructToUserQuery = knex('user_salary_structure')
    .insert({
      user_id: userId,
      salary_structure_id: salaryStructId,
      salary_structure_start_time: moment(packet.start_time).utc().format('YYYY-MM-DD'),
      salary_structure_end_time: moment(packet.end_time).utc().format('YYYY-MM-DD'),
    }).returning(['id', 'salary_structure_id', 'user_id']);
  return executeQuery(assignSalaryStructToUserQuery)
    .then((userSalaryStructure) => {
      if (userSalaryStructure && userSalaryStructure.rows.length) {
        return `Salary structure having id '${salaryStructId}' has been assigned to user having id '${userId}' successfully`;
      }
    });
};

exports.deleteSalaryStructForUserById = (userSalaryStructId) => {
  const deleteUserSalaryStructByIdQuery = knex('user_salary_structure')
    .where('id', userSalaryStructId)
    .del();
  return executeQuery(deleteUserSalaryStructByIdQuery)
    .then((deletedUserSalaryStruct) => {
      if (!deletedUserSalaryStruct.rows.length) {
        return `User salary structure having id '${userSalaryStructId}' deleted successfully`;
      }
    });
};

exports.getUserSalaryStructures = (itemsPerPage, offset) => {
  const getUserSalaryStructuresQuery = knex('user_salary_structure')
    .select('id', 'user_id', 'salary_structure_id', 'salary_structure_start_time', 'salary_structure_end_time', 'created_at', 'updated_at')
    .limit(itemsPerPage)
    .offset(offset);
  return executeQuery(getUserSalaryStructuresQuery)
    .then((userSalaryStructures) => {
      if (userSalaryStructures && userSalaryStructures.rows && userSalaryStructures.rows.length) {
        return userSalaryStructures.rows;
      }
    });
};

exports.getAllUserSalaryStructCount = () => {
  const countOfAllUserSalaryStructQuery = knex('user_salary_structure').count('*');
  return executeQuery(countOfAllUserSalaryStructQuery)
    .then((userSalaryStructCount) => {
      if (userSalaryStructCount && userSalaryStructCount.rows && userSalaryStructCount.rows.length) {
        return parseInt(userSalaryStructCount.rows[0].count, 10);
      }
    });
};

exports.getSalaryStructureId = (userId, salaryDateUTC) => {
  const getSalaryStructureIdQuery = knex('user_salary_structure')
    .select('id')
    .where('user_id', userId)
    .andWhere('salary_structure_start_time', '<=', salaryDateUTC)
    .andWhere('salary_structure_end_time', '>=', salaryDateUTC)
    .limit(1);
  return executeQuery(getSalaryStructureIdQuery)
    .then((userSalaryStructure) => {
      if (userSalaryStructure && userSalaryStructure.rows && userSalaryStructure.rows.length) {
        return userSalaryStructure.rows[0];
      }
    });
};
