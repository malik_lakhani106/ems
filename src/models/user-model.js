const { v4 } = require('uuid');
const bcrypt = require('bcryptjs');
const { executeQuery, knex } = require('../providers/db-connections.js');

exports.createUser = (packet) => {
  const tempPacket = packet;
  delete tempPacket.role_id;
  if (packet.department_id) {
    delete tempPacket.department_id;
  }
  tempPacket.confirmation_code = v4();
  const createUserQuery = knex('users')
    .insert(tempPacket)
    .returning(['id', 'user_email', 'confirmation_code']);
  return executeQuery(createUserQuery)
    .then((user) => {
      if (user.rows && user.rows.length) {
        return user.rows[0];
      }
    });
};

exports.getCurrentLoggedInUser = (loggedInUserId) => {
  const getCurrentLoggedInUserQuery = knex('users').select('id', 'first_name', 'middle_name', 'last_name', 'user_email', 'user_address', 'phone_number', 'company_id')
    .where('id', loggedInUserId)
    .limit(1);
  return executeQuery(getCurrentLoggedInUserQuery)
    .then((currentLoggedInUser) => { 
      if (currentLoggedInUser.rows && currentLoggedInUser.rows.length) {
        return currentLoggedInUser.rows[0];
      }
    });
};

exports.getUserByEmail = (userEmail) => {
  const selectUserByEmailQuery = knex('users')
    .select('user_email')
    .limit(1)
    .where('user_email', userEmail);
  return executeQuery(selectUserByEmailQuery)
    .then((user) => {
      if (user && user.rows) {
        return user.rows;
      }
    });
};

exports.updateUserById = (userId, packet) => {
  const tempPacket = packet;
  if (packet.department_id) {
    delete tempPacket.department_id;
  }
  if (packet.role_id) {
    delete tempPacket.role_id;
  }
  const updateUserByIdQuery = knex('users')
    .update(tempPacket)
    .where('id', userId);
  return executeQuery(updateUserByIdQuery)
    .then((updatedUser) => {
      if (updatedUser && updatedUser.rows && updatedUser.rows.length) {
        return updatedUser.rows[0];
      }
    });
};

exports.confirmUser = (email, password) => bcrypt.genSalt(10)
  .then((salt) => bcrypt.hash(password, salt)
    .then((hash) => {
      const updateUserByEmailQuery = knex('users')
        .update({
          user_password: hash,
          is_active: true,
        }, ['id', 'is_active'])
        .where('user_email', email);
      return executeQuery(updateUserByEmailQuery)
        .then((updatedUser) => {
          if (updatedUser && updatedUser.rows.length && updatedUser.rows[0].is_active) {
            return `User having email '${email}' has been activated successfully`;
          }
        });
    }));

exports.deleteUserById = (userId) => {
  const deleteUserByIdQuery = knex('users')
    .where('id', userId)
    .del();
  return executeQuery(deleteUserByIdQuery)
    .then((deletedUser) => {
      if (!deletedUser.rows.length) {
        return `User having id '${userId}' deleted successfully`;
      }
    });
};
