const { executeQuery, knex } = require('../providers/db-connections.js');

exports.getDocuments = (itemsPerPage, offset, userId) => {
  const getDocumentsQuery = knex('user_document')
    .select('file_name')
    .limit(itemsPerPage)
    .offset(offset)
    .where('user_id', userId);
  return executeQuery(getDocumentsQuery)
    .then((documents) => {
      if (documents && documents.rows) {
        return documents.rows;
      }
    });
};

exports.getDocumentsCount = (userId) => {
  const getDocumentsCountQuery = knex('user_document').count('*').where('user_id', userId);
  return executeQuery(getDocumentsCountQuery)
    .then((documentCount) => {
      if (documentCount && documentCount.rows) {
        return parseInt(documentCount.rows[0].count, 10);
      }
    });
};

exports.uploadDocument = (userId, fileName) => {
  const uploadDocumentQuery = knex('user_document')
    .insert({
      user_id: userId,
      file_name: fileName,
      file_path: `/static/users/${userId}/${fileName}`,
    })
    .returning('*');
  return executeQuery(uploadDocumentQuery)
    .then((result) => {
      if (result && result.rows && result.rows.length) {
        return 'File uploaded successfully';
      }
    });
};

exports.downloadDocument = (documentId, userId) => {
  const downloadDocumentQuery = knex('user_document')
    .select('file_name')
    .where('id', documentId)
    .andWhere('user_id', userId)
    .limit(1);
  return executeQuery(downloadDocumentQuery)
    .then((document) => {
      if (document && document.rows && document.rows.length) {
        return document.rows[0];
      }
    });
};

exports.deleteDocument = (documentId, userId) => {
  const deleteDocumentQuery = knex('user_document')
    .del()
    .where('id', documentId)
    .andWhere('user_id', userId);
  return executeQuery(deleteDocumentQuery)
    .then((deletedDocument) => {
      if (!deletedDocument.rows.length) {
        return 'File deleted successfully';
      }
    });
};
