const { executeQuery, knex } = require('../providers/db-connections.js');

exports.createUserRole = (userId, roleId) => {
  const createUserRoleQuery = knex('user_role')
    .insert({
      user_id: userId,
      role_id: roleId,
    }).returning('id');
  return executeQuery(createUserRoleQuery)
    .then((userRole) => {
      if (userRole && userRole.rows.length) {
        return userRole.rows[0];
      }
    });
};

exports.updateUserRoleById = (userId, roleId) => {
  const updateUserRoleByIdQuery = knex('user_role')
    .update({
      role_id: roleId,
    })
    .where('user_id', userId);
  return executeQuery(updateUserRoleByIdQuery)
    .then((updatedUserRole) => {
      if (updatedUserRole && updatedUserRole.rows && updatedUserRole.rows.length) {
        return updatedUserRole.rows[0];
      }
    });
};
