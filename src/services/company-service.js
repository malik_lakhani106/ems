const {
  getCompanyByName, createCompany, getCompaniesPerPage, getAllCompaniesCount, updateCompanyById, deleteCompanyById,
} = require('../models/company-model.js');

exports.createCompany = (packet) => getCompanyByName(packet.company_name)
  .then((company) => {
    if (company && company.length) {
      const err = new Error();
      err.errorType = 'COMPANY_ALREADY_EXISTS';
      err.errorMessage = `Company with name '${company[0].company_name}' already exists`;
      err.statusCode = 400;
      throw err;
    } else {
      return createCompany(packet.company_name)
        .then((company) => {
          if (company) {
            return company;
          }
        });
    }
  });

exports.getCompanies = (itemsPerPage, offset) => Promise.all([getCompaniesPerPage(itemsPerPage, offset), getAllCompaniesCount()]).then((results) => results);

exports.updateCompanyById = (companyName, companyId) => getCompanyByName(companyName)
  .then((company) => {
    if (company && company.length) {
      const err = new Error();
      err.errorType = 'COMPANY_ALREADY_EXISTS';
      err.errorMessage = `Cannot update company as company with name '${company[0].company_name}' already exists`;
      throw err;
    } else {
      return updateCompanyById(companyName, companyId).then((updatedCompany) => {
        if (updatedCompany) {
          return updatedCompany;
        }
      });
    }
  });

exports.deleteCompanyById = (companyId) => deleteCompanyById(companyId).then((deletedCompany) => {
  if (deletedCompany) {
    return deletedCompany;
  }
});
