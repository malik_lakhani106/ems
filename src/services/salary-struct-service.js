const {
  createSalaryStruct, getSalaryStructureByName, updateSalaryStructureById, deleteSalaryStructureById, getAllSalaryStructures, getAllSalaryStructuresCount,
} = require('../models/salary-struct-model.js');

exports.createSalaryStruct = (packet) => getSalaryStructureByName(packet.salary_structure_name)
  .then((salaryStructure) => {
    if (salaryStructure) {
      const err = new Error();
      err.statusCode = 400;
      err.errorType = 'SALARY_STRUCTURE_ALREADY_EXISTS';
      err.errorMessage = `Salary structure with name '${packet.salary_structure_name}' already exists`;
      throw err;
    } else {
      return createSalaryStruct(packet)
        .then((salaryStructure) => {
          if (salaryStructure) {
            return salaryStructure;
          }
        });
    }
  });

exports.updateSalaryStructureById = (salaryStructureId, packet) => updateSalaryStructureById(salaryStructureId, packet)
  .then((updatedSalaryStructure) => {
    if (updatedSalaryStructure && updatedSalaryStructure.length) {
      return 'Salary structure updated successfully';
    }
  });

exports.deleteSalaryStructureById = (salaryStructureId) => deleteSalaryStructureById(salaryStructureId)
  .then((deletedSalaryStructure) => {
    if (deletedSalaryStructure) {
      return deletedSalaryStructure;
    }
  });

exports.getAllSalaryStructures = (itemsPerPage, offset) => Promise.all([getAllSalaryStructures(itemsPerPage, offset), getAllSalaryStructuresCount()]).then((results) => results);
