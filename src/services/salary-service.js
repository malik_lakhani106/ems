const moment = require('moment');
const { getSalaryStructureId } = require('../models/user-salary-struct-model.js');
const { createSalary, getAllSalaryCountByUserId, getSalaries } = require('../models/salary-model.js');

exports.createSalary = (userId, packet) => {
  const salaryDateUTC = moment(packet.salary_date).utc().format('YYYY-MM-DD');
  return getSalaryStructureId(userId, salaryDateUTC)
    .then((userSalaryStructure) => {
      if (userSalaryStructure) {
        return createSalary(userId, userSalaryStructure.id, salaryDateUTC)
          .then((userSalary) => {
            if (userSalary) {
              return userSalary;
            }
          });
      }
      const err = new Error();
      err.errorType = 'INVALID_SALARY_CREATION';
      err.errorMessage = `Cannot create salary for user having id '${userId}' as there is not salary structure assigned for the date '${packet.salary_date}'`;
      err.statusCode = 400;
      throw err;
    });
};

exports.getSalaries = (userId, packet, itemsPerPage, offset) => Promise.all([getSalaries(userId, packet, itemsPerPage, offset), getAllSalaryCountByUserId(userId)]).then((results) => results);
