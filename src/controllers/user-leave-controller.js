const { createUserLeave, updateUserLeave, deleteUserLeave } = require('../services/user-leave-service.js');

const defaultErrorType = 'INTERNAL_SERVER_ERROR';

const defaultErrorMsg = 'Something went wrong. Please try again later';

exports.createUserLeave = (req, res) => {
  createUserLeave(req.params.uid, req.body)
    .then((userLeave) => {
      res.status(200)
        .json({
          status: userLeave,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.updateUserLeave = (req, res) => {
  updateUserLeave(req.params.ulid, req.body)
    .then((updatedUserLeave) => {
      res.status(200)
        .json({
          status: updatedUserLeave,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.deleteUserLeave = (req, res) => {
  deleteUserLeave(req.params.ulid)
    .then((deletedUserLeave) => {
      res.status(200)
        .json({
          status: deletedUserLeave,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};
