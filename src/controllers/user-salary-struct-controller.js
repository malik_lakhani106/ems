const { assignSalaryStructToUser, deleteSalaryStructForUserById, getUserSalaryStructures } = require('../services/user-salary-struct-service.js');

const defaultErrorType = 'INTERNAL_SERVER_ERROR';

const defaultErrorMsg = 'Something went wrong. Please try again later';

exports.assignSalaryStructToUser = (req, res) => {
  assignSalaryStructToUser(req.params.uid, req.params.ssid, req.body)
    .then((userSalaryStructure) => {
      res.status(200)
        .json({
          message: userSalaryStructure,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.deleteSalaryStructForUserById = (req, res) => {
  deleteSalaryStructForUserById(req.params.ussid)
    .then((deletedUserSalaryStruct) => {
      res.status(200).json({
        message: deletedUserSalaryStruct,
      });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.getUserSalaryStructures = (req, res) => {
  const defaultItemsPerPage = 10;
  const defaultOffset = 0;
  getUserSalaryStructures(req.query.itemsPerPage || defaultItemsPerPage, req.query.offset || defaultOffset)
    .then((userSalaryStructures) => {
      res.status(200)
        .json({
          user_salary_structures: userSalaryStructures[0],
          metadata: {
            total: userSalaryStructures[1],
            itemsPerPage: parseInt(req.query.itemsPerPage, 10) || defaultItemsPerPage,
            offset: parseInt(req.query.offset, 10) || defaultOffset,
          },
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};
