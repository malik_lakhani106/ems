const Joi = require('@hapi/joi');

exports.validateCreateCompany = (request, response, next) => {
  const packet = {
    ...request.body,
  };

  const schema = Joi.object({
    company_name: Joi.string().min(3).required(),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateUpdateCompany = (request, response, next) => {
  const packet = {
    ...request.params,
    ...request.body,
  };

  const schema = Joi.object({
    cid: Joi.number().integer().min(1),
    company_name: Joi.string().min(3).required(),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateDeleteCompany = (request, response, next) => {
  const packet = {
    ...request.params,
  };

  const schema = Joi.object({
    cid: Joi.number().integer().min(1),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};
