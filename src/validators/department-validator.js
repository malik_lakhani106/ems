const Joi = require('@hapi/joi');

exports.validateCreateDepartment = (request, response, next) => {
  const packet = {
    ...request.body,
  };

  const schema = Joi.object({
    department_name: Joi.string().min(3).required(),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateUpdateDepartment = (request, response, next) => {
  const packet = {
    ...request.params,
    ...request.body,
  };

  const schema = Joi.object({
    did: Joi.number().integer().min(1),
    department_name: Joi.string().min(3).required(),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateDeleteDepartment = (request, response, next) => {
  const packet = {
    ...request.params,
  };

  const schema = Joi.object({
    did: Joi.number().integer().min(1),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};
