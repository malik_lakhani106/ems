const Joi = require('@hapi/joi');

exports.validateCreateUser = (request, response, next) => {
  const packet = {
    ...request.body,
  };

  const schema = Joi.object({
    first_name: Joi.string().min(3).required(),
    user_email: Joi.string().email().required(),
    role_id: Joi.number().min(1).integer().required(),
    salutation: Joi.string().min(1).max(4),
    middle_name: Joi.string().min(3),
    last_name: Joi.string().min(3),
    user_address: Joi.string().min(10),
    phone_number: Joi.number().integer().min(1000000000).max(9999999999),
    date_of_birth: Joi.date(),
    company_id: Joi.number().integer().min(1),
    gender: Joi.string().valid('male', 'female'),
    date_of_joining: Joi.date(),
    department_id: Joi.number().integer().min(1),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateUpdateUser = (request, response, next) => {
  const packet = {
    ...request.params,
    ...request.body,
  };

  const schema = Joi.object({
    uid: Joi.number().integer().min(1),
    salutation: Joi.string().min(1).max(4),
    first_name: Joi.string().min(3),
    middle_name: Joi.string().min(3),
    last_name: Joi.string().min(3),
    role_id: Joi.number().integer().min(1),
    user_address: Joi.string().min(10),
    phone_number: Joi.number().integer().min(1000000000).max(9999999999),
    date_of_birth: Joi.date(),
    company_id: Joi.number().integer().min(1),
    gender: Joi.string().valid('male', 'female'),
    date_of_joining: Joi.date(),
    department_id: Joi.number().integer().min(1),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateConfirmUser = (request, response, next) => {
  const packet = {
    ...request.body,
  };

  const schema = Joi.object({
    email: Joi.string().email().required(),
    confirmation_code: Joi.string().pattern(new RegExp('^[a-z0-9-]*$')).required(),
    password: Joi.string().min(6).max(20).required(),
    confirm_password: Joi.string().required().valid(Joi.ref('password')),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateDeleteUser = (request, response, next) => {
  const packet = {
    ...request.params,
  };

  const schema = Joi.object({
    uid: Joi.number().integer().min(1),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};
