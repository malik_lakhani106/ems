const Joi = require('@hapi/joi');

exports.validateAuthLogin = (request, response, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(6).max(20).required(),
  });

  schema.validateAsync(request.body).then((result) => {
    if (result) {
      next();
    }
  }).catch((err) => {
    response.status(400)
      .json({
        errorType: 'VALIDATION_ERROR',
        errorMessage: err.details[0].message,
      });
  });
};
