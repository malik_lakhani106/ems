const { executeQuery, knex } = require('../providers/db-connections.js');

exports.checkDepartmentExistsById = (request, response, next) => {
  if (!(request.body.department_id)) {
    return next();
  }
  const checkDepartmentByIdQuery = knex('departments')
    .select('department_name')
    .where('id', request.params.did || request.body.department_id)
    .limit(1);
  executeQuery(checkDepartmentByIdQuery)
    .then((department) => {
      if (department && department.rows.length) {
        next();
      } else {
        response.status(404)
          .json({
            errorType: 'DEPARTMENT_NOT_FOUND',
            errorMessage: `Department having id '${request.params.did || request.body.department_id}' does not exists`,
          });
      }
    });
};
